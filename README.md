# AAX To MP3 Docker image

Dockerisation of AAXtoMP3 tools which you can found here: https://github.com/KrumpetPirate/AAXtoMP3

This Dockerisation is specialized for my own need but feel free to fork and improve 

## What will it do

It will transform AAXC files onto a list of MP3 chapters with the book cover and a m3u playlist

## How to use this image
### Prepare
You first need to download your audible library as AAXC files, you can do it using this tools : https://github.com/mkb79/audible-cli
#### Download audible-cli 
First download and extract the last version of audible-cli for windows: https://github.com/mkb79/audible-cli/releases/download/v0.1.0/audible_win.zip

#### Connect you audible account
- Open a terminal then go to the folder where you extract the tools.
- Run this command: `.\audible.exe quickstart`
- then follow the prompts

#### Download your audiobook as AAXC files
Once your connect to your audible account you can donwload your audiobook, you can either donwload all books or specify the ones you want

##### Download all books in you library

The following command will download in /path/to/download all the audiobook in your library with chapters information and cover

`.\audible.exe download --aaxc --cover --cover-size 1215 --chapter --all -o /path/to/download`

##### Download specific books in you library
To do this you first need to download a file that list all the book in you library with the following command:

`.\audible.exe library export`

This will save a file 'library.tsv' in your current folder

It contains all the information of your audiobooks, the data we need is the one in the first column the ASIN code 

You can now use ASIN code to download one book only 

The following command will download in /path/to/download the audiobook corresponding to the ASIN_CODE in your library with chapters information and cover

`.\audible.exe download --aaxc --cover --cover-size 1215 --chapter -a ASIN_CODE -o /path/to/download`

### Transform your AAXC

Once you download your audiobooks you can transform them to MP3 files.

To do this you need to:
- create the destination folder
- run this docker command : `docker run -v AUDIBLE_CONFIG_FOLDER:/root/.audible:ro -v PATH_FROM:/media/from:ro -v PATH_TO:/media/to --name AaxToMp3 registry.gitlab.com/wearelautre/utils/aaxtomp3-docker` 

*AUDIBLE_CONFIG_FOLDER* : 

| OS        | Folder                                    |
|-----------|-------------------------------------------|
| WINDOWS   | `C:\Users\<user>\AppData\Local\audible`   |
| MAC       | `~/.audible`                              |
| LINUX     | `~/.audible`                              |
