FROM ubuntu:22.04

RUN mkdir app
WORKDIR /app

ARG AAX_TO_MP3_VERSION=1.3

ENV AAX_TO_MP3_VERSION=$AAX_TO_MP3_VERSION
ENV DEBUG_LEVEL=0
ENV TARGET_FORMAT="\$artist/\$title"

RUN apt-get update && apt-get install -y software-properties-common

RUN add-apt-repository ppa:savoury1/ffmpeg4 && \
    apt-get update && \
    apt-get install -y ffmpeg mediainfo wget jq ffmpeg x264 x265 bc && \
    wget https://www.deb-multimedia.org/pool/main/m/mp4v2-dmo/libmp4v2-2_2.0.0-dmo6_amd64.deb && \
    wget https://www.deb-multimedia.org/pool/main/m/mp4v2-dmo/mp4v2-utils_2.0.0-dmo6_amd64.deb && \
    dpkg -i libmp4v2-2_2.0.0-dmo6_amd64.deb && \
    dpkg -i mp4v2-utils_2.0.0-dmo6_amd64.deb

RUN wget -O AAXtoMP3.tar.gz https://github.com/KrumpetPirate/AAXtoMP3/releases/download/v$AAX_TO_MP3_VERSION/AAXtoMP3-v$AAX_TO_MP3_VERSION.tar.gz && \
    tar -xvzf AAXtoMP3.tar.gz

VOLUME /root/.audible
VOLUME /media/from
VOLUME /media/to

CMD bash /app/AAXtoMP3/AAXtoMP3 --level 9 -l $DEBUG_LEVEL --use-audible-cli-data -D ../to/$(echo $TARGET_FORMAT) /media/from/*.aaxc